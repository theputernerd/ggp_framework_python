# GGP_Framework_Python 3

This is a general game playing framework for python which I use for my PhD research into General Decision making. It can be used as the API for other games or the whole game engine. If you create a game within this framework why not share it here. 
 
It is opensource and free to use, but citing this repository would be appreciated if it were appropriate or if you did something awesome from it.

Have a look at play_game.py to see an example of how to use the games.

 -env contains the game environments. To make your own environment just inherit from env_inherit_from.py and fill in the methods.
 -lib contains small library of helper functions
 -player contains the different players. To make your own player just inherit from player_inherit_from.py and fill in the methods. Random and human players are already implemented as examples.

Let me know if you think I've missed something as I trimmed this down for example purposes. 

To play a game there is a playonegame function in lib.helpers which takes parameters (environment, player,player). You just create instances of the environment and each player and the function will play a game between the two players. 
