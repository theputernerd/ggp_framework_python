
from env.env_inherit_from import Environment
from player.player_inherit_from import i_Player
import numpy as np
import sys
import enum


def playOneGame(env:Environment, playera:i_Player, playerb:i_Player, printWinner=False, printMoves=False):
    """
    plays one game of env between playera and player b. Returns the winner.
    :param env: the game environment
    :param playera: the first player. changing player.playing_as changes when the order they move
    :param playerb: the second player
    :param printWinner: prints the winner before returning if True
    :param printMoves: print each move and renders between moves if True
    :return: an int representing the winner or 3 for a draw.
    """
    playerb.playing_as = 3 - playera.playing_as
    if printMoves:
        print("Starting Game --------------------------------------------------------------------------------")
        env.render()
        playera.new_game()
        playerb.new_game()

    while not env.done:
        #env.render()
        t = env.next_player
        if t == playera.playing_as:
            action = playera.get_move(env.clone())
            playername=playera.shortName
        elif t == playerb.playing_as:
            action = playerb.get_move(env.clone())
            playername = playerb.shortName
        else:
            assert False  # turn doesn't match a player.

        env.step(action)

        if printMoves:
            print("-----------")
            print(f"{playername} move: {action}")
            env.render()
            print(f"Key{env.key}")

        sys.stdout.flush()
    winner=env.winner
    playera.finished_game(env.key,winner)
    playerb.finished_game(env.key,winner)
    if printWinner:
        env.render()
        if winner==playera.playing_as:
            print(f"{playera.shortName} Wins as player {playera.playing_as}")
        elif winner == playerb.playing_as:
            print(f"{playerb.shortName} Wins as player {playerb.playing_as}")
        elif winner==3:
            print(f"{playera.shortName} playing as {playera.playing_as}")
    return winner
##################################Some common helpers for using bit representations for a board
def bit_count(x):
    return bin(x).count('1')
def bit_to_array(x, size):
    try:
        b=bin(x)[2:]
        #b=bin(((1 << 32) - 1) & -5)
    except:
        print(x)
        raise
    r=list((("0" * size) + b)[-size:])
    rreversed=reversed(r)

    npArray=np.array(list(rreversed), dtype=np.uint8)

    return npArray

