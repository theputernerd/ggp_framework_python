from env.connect4_env import Connect4Env
from env.tictactoe import TTT
from env.reversi_env import ReversiEnv
from env.ChessE import Chess

from player.player_human import Human_Player
from player.player_random import Random_Player

from lib.helpers import playOneGame
#################################
#How to play a game
#env=TTT()

print("------------------Reversi Random vs Random-------------------------------------------------")
env = ReversiEnv()
p1 = Random_Player(env, playing_as=1)
p2 = Random_Player(env, playing_as=2)
winner = playOneGame(env, p1, p2, printMoves=True,printWinner=False)
print("------------------Connect 4 Random vs Random-------------------------------------------------")
env = Connect4Env(width=5, height=5)
p1 = Random_Player(env, playing_as=1)
p2 = Random_Player(env, playing_as=2)
winner = playOneGame(env, p1, p2, printMoves=True,printWinner=False)
print("---------------Planes--------------")
print(env.planes)
print("------------------Connect4 Full Human Vs Random-------------------------------------------------")
env=Connect4Env()
p1=Human_Player(env,playing_as=2)
p2=Random_Player(env,playing_as=1)
winner=playOneGame(env,p1,p2,printMoves=True,printWinner=True)
print("---------------Planes--------------")
print(env.planes)
print("------------------Tic Tac Toe Human Vs Random-------------------------------------------------")
env = TTT()
p1 = Human_Player(env, playing_as=2)
p2 = Random_Player(env, playing_as=1)
winner = playOneGame(env, p1, p2, printMoves=True, printWinner=True)
print("---------------Planes--------------")
print(env.planes)
assert False
#only if you have python chess installed. pip install python-chess of pip install chess
print("------------------Chess Random vs Random-------------------------------------------------")
env = Chess()
p1 = Random_Player(env, playing_as=1)
p2 = Random_Player(env, playing_as=2)
winner = playOneGame(env, p1, p2, printMoves=True,printWinner=False)
#################################
