from unittest import TestCase

from env.tictactoe import TTT
from env.connect4_env import Connect4Env
from env.reversi_env import ReversiEnv
from env.ChessE import Chess
#############################################
# TO USE THESE TESTS JUST CHANGE THE env_ptr to the environment you want tested.
env_ptr = Chess#ReversiEnv#Connect4Env#Connect4_5Env #TTT  # this is the environment to test
##################################################
import logging
logger = logging.getLogger('env_tests')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('env_tests.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
env=env_ptr()
logger.info(f"----------------------{env_ptr}----------Starting Env Tests--------------------------------------------------------------------")
import random

params = ['width', 'height', 'n_cells', 'name', 'next_player', 'key', 'shortname', 'n_planes', 'n_actions', 'done', 'winner',
          ]
class TestEnvironment(TestCase):
    def testInstantiation(self):
        logger.info("--testInstantiation ----------------------")
        env=env_ptr()
        logger.info(f"{env_ptr} instantiates")
        self.assertRaises(Exception)

    def test_get_legal_moves(self):
        import numpy as np
        #get_legal_moves should return a list.
        logger.info("--test_get_legal_moves ----------------------")
        env = env_ptr()
        moves=env.legal_moves_list()
        val=(list==type(moves)) or (type(moves)==np.ndarray)
        self.assertTrue(val,f"get_legal_moves should return a list or ndarray of legal moves. {moves}")

    def test_clone(self):
        logger.info("--Clone Test ----------------------")
        env=env_ptr()
        env2 = env.clone()
        self.assertNotEqual(env,env2,msg="cloned ptr is same as original.")
        global params
        str="Checking variables env.:"
        for p in params:
            str+=f"{p}, "
            call1=getattr(env,p)
            call2=getattr(env2,p)
            self.assertEqual(call1,call2,msg=f"{p} not cloned")

        logger.info(str)
        call1=repr(env)
        call2=repr(env2)
        self.assertEqual(call1, call2, msg=f"strings not the same")
        logger.info("now doing 2 moves then checking")
        moves=env.legal_moves_list()
        m=random.choice(moves)
        env.step(m)
        moves=env.legal_moves_list()
        m=random.choice(moves)
        env.step(m)
        env2 = env.clone()
        self.compareTwoEnvirons(env,env2)
    def test_step(self):
        logger.info("--test_step ----------------------")
        env = env_ptr()
        s=repr(env)
        s+="\n"
        m=env.legal_moves_list()[0]
        s += f"\n move:{m}\n"
        env.step(m)
        s+=repr(env)
        logger.info(s)
        pass

    def test_get_result_is_terminal(self):
        logger.info("--test_get_result ----------------------")
        env=env_ptr()
        while not env.is_terminal():
            result = env.get_result()
            self.assertEqual(result, 0, "get_result should return 0 if game is still going")
            moves=env.legal_moves_list()
            move=random.choice(moves)
            env.step(move)

        result=env.get_result()
        self.assertNotEqual(0,result,msg=f"get_result shouldn't be 0 if game is over.{env}") #if game is over

    def test_reset(self):
        logger.info("--test_reset ----------------------")
        env2 = env_ptr()
        env= env_ptr()
        moves = env.legal_moves_list()
        move = random.choice(moves)
        env.step(move)
        env.reset()
        str = "Checking variables env.:"
        for p in params:
            str += f"{p}, "
            call1 = getattr(env, p)
            call2 = getattr(env2, p)
            self.assertEqual(call1, call2, msg=f"{p} not cloned")
        logger.info(str)
        ##########Now test the board cloned properly
        logger.info(str)
        # check if pointers are same
        self.assertNotEqual(env, env2, "Board ptr is identical in reset")
    def compareTwoEnvirons(self,env,env2):
        global params
        str = "Checking variables env.:"
        for p in params:
            str += f"{p}, "
            call1 = getattr(env, p)
            call2 = getattr(env2, p)
            self.assertEqual(call1, call2, msg=f"{p} not identical")

        ##########Now test the board cloned properly
        logger.info(str)
        # check if pointers are same
        self.assertNotEqual(env, env2, "Board ptr is identical")

    def test_update(self):
        logger.info("--test_update ----------------------")
        env=env_ptr()
        count=0
        while not env.is_terminal():
            if count>2: break
            count+=1
            result = env.get_result()
            self.assertEqual(result, 0, "get_result should return 0 if game is still going")
            moves=env.legal_moves_list()
            move=random.choice(moves)
            env.step(move)
        env2 = env_ptr()
        env2.update(env.key)
        self.compareTwoEnvirons(env,env2)

    def test_key(self):
        logger.info("--test_key ----------------------")
        env = env_ptr()
        moves = env.legal_moves_list()
        move = random.choice(moves)
        env.step(move)
        k=env.key
        self.assertTrue(isinstance(k, str),"They key is not a string. It must be")

    def test_render(self):
        logger.info("--test_render ----------------------")
        env = env_ptr()
        env.render()

    def test_planes(self):
        env=env_ptr()
        import numpy as np
        p=env.planes
        self.assertTrue(type(p)==np.ndarray)

        sh=p.shape
        self.assertEqual(env.n_planes,sh[0],msg="env.n_planes does not match number of planes")
        self.assertEqual(env.height,sh[1],msg="env.height does not match number of planes")
        self.assertEqual(env.width,sh[2],msg="env.width does not match number of planes")


