import json
from abc import ABCMeta, abstractmethod
import numpy as np
class Environment(object,metaclass=ABCMeta):
    def __init__(self,width:int,height:int,n_inputs:int,n_actions:int,n_planes:int,name=None,shortname=None):
        """
        While these parameters define the game, they are also used to create the neural network.
        :param width: how wide the board is
        :param height: how high the board is
        :param n_inputs: how many inputs for the neural network for this board - usually height*width
        :param n_actions: how many unique actions are there in the game. eg for Chess there is >4000. Connect4 it's width
        :param n_planes: how many planes will a position be represented by - generally a minimum of 2 [current player pieces, enemy pieces] Chess has a plane for each players piece
        :param name: <optional> name of the player. Useful for distinguising different players
        :param shortname: <optional> Useful for logging and filenames etc. Recommend shortname is filename safe
        """
        self.done = False
        self.winner = None  # 0 still playing, 1 p1 wins, 2 p2, 3 draw
        self.width=width
        self.height=height
        self.name = name
        self.shortname = name if shortname==None else shortname
        self.n_cells=n_inputs
        self.n_planes=n_planes #This is how many bit planes will be create for a game position.
                                    # Used to create the neural network architecture
        self.n_actions=n_actions #this is the maximum number of actions. eg in Chess n_actions is > 4000.
                                    # Used to create the neural network architecture
        self.next_player = 1  # type: Player

    @abstractmethod
    def clone(self):
        """
        clones an environment. Do this if you don't want to change it.
        You could also just get the key and keep it and recreate the env.
        :return: a clone of the game environment
        """
        description = "You need to override the clone method in env. "
        raise Exception('method not over-ridden', __file__, description)

    @abstractmethod
    def get_result(self)->int:
        """
        The result of the game.
        returns the winner (1,2) or 3 for a draw if game is over - 0 if still going
        :return: int [0,1,2,3]
        """
        description = "You need to override the get_result method in env. It returns a single integer - 0 game still going, 1 p1wins,2p2wins 3 draw."
        raise Exception('method not over-ridden', __file__, description)

    @abstractmethod
    def is_terminal(self)->bool:
        """
        Indicates if game is over. It is only set after step or update is called.
        If the env is changed outside of step or update then it may not be accurate.
        :return: True if game is over otherwise False
        """
        description = "You need to override the is_terminal method in env. It returns a bool indicating if game is over. Also sets winner."
        raise Exception('method not over-ridden', __file__, description)

    @abstractmethod
    def reset(self):
        """
        Resets the environment to the start state
        :return: None
        """
        description = "You need to override the reset method in env. "
        raise Exception('method not over-ridden', __file__, description)
    @abstractmethod
    def update(self, key):
        """
        Sets the environment to the given key. After calling this env.key should be the same as key.
        key needs to fully represent the game state.
        :param key: the string representation of the environment.
        :return: None
        """
        description = "You need to override the update method in env. It updated the environment to look like board. It returns self."
        raise Exception('method not over-ridden', __file__, description)
    @property
    @abstractmethod
    def key(self)->str:  # canonical string rep of the board.
        """
        get a canonical key for the game state. When given to update it sets the environment to a precise state.
        :return: string
        """
        description = "You need to override the key property in env. ."
        raise Exception('method not over-ridden', __file__, description)
        rep = [self.board.black, self.board.white, self.board.next_player]
        key = json.dumps(rep)
        return key
    @abstractmethod
    def step(self, action): #take this step in the environment.
        """
        Takes a step with action, updates the environment
        :param action: The action to take
        :return: self.key a key for the next gamestate after the action was taken
        """
        description = "You need to override the step method in env. It updated the environment to look like board. It returns self.board,{}."
        raise Exception('method not over-ridden', __file__, description)
        return self.key

    def legal_moves_list(self):
        """
        Gets the moves which are legal as a list of integers
        :return: list of integers representing legal moves
        """
        mask = self.legal_moves_mask()
        idx = np.where(mask)[0]
        return list(idx)

    @property
    @abstractmethod
    def legal_moves_mask(self):
        """
        gets a mask of all legal move
        :return: numpy binary mask of legal moves
        """
        description = "You need to override the legal_moves method in env. It returns a list of the legal moves."
        raise Exception('method not over-ridden', __file__, description)

        return legal
    @property
    @abstractmethod
    def planes(self):
        """
        Canonical planes for neural network.
        These are defined differently depending on the game
        At a minimum [current player pieces, enemy pieces]

        :return:
        """
        description = "You need to override the planes method in env."
        raise Exception('method not over-ridden', __file__, description)

    @abstractmethod
    def render(self):
        """
        renders the environment to std out
        :return:
        """
        description = "You need to override the render method in env. It renders the board."
        raise Exception('method not over-ridden', __file__, description)
    def __repr__(self):
        description = "You need to override the __repr__ method in env."
        raise Exception('method not over-ridden', __file__, description)
        assert False