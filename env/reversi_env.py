import enum
#parts of this code from https://github.com/mokemokechicken/reversi-alpha-zero
#
from logging import getLogger

from lib.helpers import   bit_count
from env.env_inherit_from import Environment
from lib.helpers import  bit_to_array
import numpy as np
logger = getLogger(__name__)
from reversi_zero.lib.helpers import Player,Winner
import json

class ReversiEnv(Environment):
    width=8
    height=8
    def __init__(self):

        n_inputs = ReversiEnv.width * ReversiEnv.height
        n_actions = n_inputs
        n_planes = 2
        name = f"Othello_{ReversiEnv.width}x{ReversiEnv.height}"
        shortname=f"O_{ReversiEnv.width}x{ReversiEnv.height}"
        self.actionShape=(ReversiEnv.width,ReversiEnv.height)
        super().__init__(ReversiEnv.width, ReversiEnv.height, n_inputs, n_actions, n_planes, name, shortname)

        self._board = self.Board()
        self._turn = 0
        self._player_to_play = 1
        self._resigned = False
        self.next_player = 1
        self._board.next_player=self.next_player
    def _player_turn(self):
        return self._player_to_play
    @property
    def planes(self):
        return self._board.planes
    @property
    def key(self):  # abbredviated string rep of the board.
        return self._board.key

    def reset(self):
        self._board = self.Board()
        self.next_player = 1
        self._board.next_player=1
        self._turn = 0
        self.done = False
        self.winner = None
        self._player_to_play=1
        self._resigned=False

        return self

    def clone(self):
        st=ReversiEnv()
        st._board = self._board.clone()
        st.next_player = self.next_player  # type: Player
        st._turn = self._turn
        st.done = self.done
        st.winner = self.winner
        st._player_to_play=self._player_to_play
        st._resigned = self._resigned
        st.name=self.name

        return st
    def is_terminal(self):
        return self.done
    def get_result(self):
        if self.winner == Winner.black or self.winner ==1:
            return 1
        elif self.winner == Winner.white or self.winner ==2:
            return 2
        elif self.winner == Winner.draw or self.winner ==3:
            return 3
        else:
            return 0

    def checkTerm(self,changeMove=False):
        if changeMove:
            self.next_player=3-self.next_player
        wMoves= bit_count(ReversiEnv._find_correct_moves(self._board.white, self._board.black)) > 0
        bMoves= bit_count(ReversiEnv._find_correct_moves(self._board.black, self._board.white)) > 0
        if not (wMoves or bMoves):
            self._game_over()
        else:
            if changeMove:

                if self.next_player==1 and not(bMoves):
                    #then player1 is next and they dont got no moves. Let the other player go.
                    self.next_player = 3 - self.next_player
                elif self.next_player==2 and not(wMoves):
                    #then player2 is next and they dont got no moves. Let the other player go.
                    self.next_player = 3 - self.next_player
                else:
                    pass
                    #all good

        self._board.next_player=self.next_player

    def update(self, key):
        self._board = self.Board(key)
        self.next_player = self._board.next_player
        self._turn = sum(self._board.number_of_black_and_white) - 4
        self.done = False
        self.winner = None
        self.checkTerm()
        return self

    def _get_own_and_enemy(self):
        if self.next_player == 1:
            own, enemy = self._board.black, self._board.white
        else:
            own, enemy = self._board.white, self._board.black
        return own, enemy

    def step(self, action):
        """
                :param int|None action: move pos=0 ~ 63 (0=top left, 7 top right, 63 bottom right), None is resign
                :return:
                """
        #self.change_to_next_player()


        assert action is None or 0 <= action <= 63, f"Illegal action={action}. Actions {self.legal_moves_list(self._board)}"

        self._player_to_play = 3 - self._player_to_play
        if action is None:
            print("Resigned")
            self._resigned()
            self._resigned = True
            return self._board, {}

        own, enemy = self._get_own_and_enemy()

        flipped = self._calc_flip(action, own, enemy)
        if bit_count(flipped) == 0:
            self._illegal_move_to_lose(action)
            return self._board, {}
        own ^= flipped
        try:
            own |= 1 << action
        except:
            raise
        enemy ^= flipped
        own=own
        enemy=enemy
        self._set_own_and_enemy(own, enemy)
        self._turn += 1

        self.checkTerm(changeMove=True)

        self._board.next_player = self.next_player
        return self._board, {}

    def _set_own_and_enemy(self, own, enemy):
        if self.next_player == Player.black.value:
            self._board.black, self._board.white = own, enemy
        else:
            self._board.white, self._board.black = own, enemy


    def _game_over(self):
        self.done = True
        if self.winner is None:
            black_num, white_num = self._board.number_of_black_and_white
            if black_num > white_num:
                self.winner = Winner.black.value
            elif black_num < white_num:
                self.winner = Winner.white.value
            else:
                self.winner = Winner.draw.value

    def _illegal_move_to_lose(self, action):
        logger.warning(
            f"Illegal action={action}, No Flipped!{self._board.next_player} {self.legal_moves_list()}")
        #logger.warning(f"{self.legal_moves_list(self.Board(self.board.white,self.board.black,self.board.next_player))}")
        self.render()
        self._win_another_player()
        self._game_over()
    def _win_another_player(self):
        win_player = 3-self.next_player  # type: Player
        if win_player == Player.black.value:
            self.winner = Winner.black.value
        else:
            self.winner = Winner.white.value

    @staticmethod
    def _search_offset_left(own, enemy, mask, offset):
        e = enemy & mask
        blank = ~(own | enemy)
        t = e & (own >> offset)
        t |= e & (t >> offset)
        t |= e & (t >> offset)
        t |= e & (t >> offset)
        t |= e & (t >> offset)
        t |= e & (t >> offset)  # Up to six stones can be turned at once
        return blank & (t >> offset)  # Only the blank squares can be started

    @staticmethod
    def _search_offset_right(own, enemy, mask, offset):
        e = enemy & mask
        blank = ~(own | enemy)
        t = e & (own << offset)
        t |= e & (t << offset)
        t |= e & (t << offset)
        t |= e & (t << offset)
        t |= e & (t << offset)
        t |= e & (t << offset)  # Up to six stones can be turned at once
        return blank & (t << offset)  # Only the blank squares can be started

    @staticmethod
    def _find_correct_moves(own, enemy):
        """return legal moves"""
        left_right_mask = 0x7e7e7e7e7e7e7e7e  # Both most left-right edge are 0, else 1
        top_bottom_mask = 0x00ffffffffffff00  # Both most top-bottom edge are 0, else 1
        mask = left_right_mask & top_bottom_mask
        mobility = 0
        mobility |= ReversiEnv._search_offset_left(own, enemy, left_right_mask, 1)  # Left
        mobility |= ReversiEnv._search_offset_left(own, enemy, mask, 9)  # Left Top
        mobility |= ReversiEnv._search_offset_left(own, enemy, top_bottom_mask, 8)  # Top
        mobility |= ReversiEnv._search_offset_left(own, enemy, mask, 7)  # Top Right
        mobility |= ReversiEnv._search_offset_right(own, enemy, left_right_mask, 1)  # Right
        mobility |= ReversiEnv._search_offset_right(own, enemy, mask, 9)  # Bottom Right
        mobility |= ReversiEnv._search_offset_right(own, enemy, top_bottom_mask, 8)  # Bottom
        mobility |= ReversiEnv._search_offset_right(own, enemy, mask, 7)  # Left bottom
        return mobility

    def legal_moves_mask(self):  ####JOE TO CHECK THIS. I WROTE IT>
        #self.render()
        board=self._board
        if board.next_player==Player.black.value:
            legal_moves = ReversiEnv._find_correct_moves(board.black, board.white) #
        elif board.next_player==Player.white.value:
            legal_moves = ReversiEnv._find_correct_moves(board.white,
                                                         board.black)
        legal_moves_arr=bit_to_array(legal_moves, 64)
        return legal_moves_arr

    def legal_moves_list(self): ##JOE WROTE THIS
        lmoves = self.legal_moves_mask()
        l_moves_idx = np.nonzero(lmoves)#np.nonzero(bit_to_array(lmoves, self.n_actions))[0]
        l_moves_idx=list(l_moves_idx[0])
        numbers = [int(x) for x in l_moves_idx]
        return numbers


    def _resigned(self):
        self._win_another_player()
        self._game_over()

    def render(self):
        b, w = self._board.number_of_black_and_white
        print(f"next={self.next_player} turn={self._turn} B={b} W={w}")
        print(self._board)

    @staticmethod
    def _action_to_val(val):  # converts a action into a single int for a flattened action array
        return val

    @staticmethod
    def _val_to_action(val):

        return val

    @staticmethod
    def _calc_flip(pos, own, enemy):
        """return flip stones of enemy by bitboard when I place stone at pos.
        :param pos: 0~63
        :param own: bitboard (0=top left, 63=bottom right)
        :param enemy: bitboard
        :return: flip stones of enemy when I place stone at pos.
        """
        assert 0 <= pos <= 63, f"pos={pos}"
        f1 = ReversiEnv._calc_flip_half(pos, own, enemy)
        f2 = ReversiEnv._calc_flip_half(63 - pos, ReversiEnv._rotate180(own), ReversiEnv._rotate180(enemy))
        return f1 | ReversiEnv._rotate180(f2)

    @staticmethod
    def _calc_flip_half(pos, own, enemy):
        el = [enemy, enemy & 0x7e7e7e7e7e7e7e7e, enemy & 0x7e7e7e7e7e7e7e7e, enemy & 0x7e7e7e7e7e7e7e7e]
        masks = [0x0101010101010100, 0x00000000000000fe, 0x0002040810204080, 0x8040201008040200]
        #a=0x8040201008040200
        #m1=a<<int(pos)
        masks = [ReversiEnv._b64(m << pos) for m in masks]
        flipped = 0
        for e, mask in zip(el, masks):
            outflank = mask & ((e | ~mask) + 1) & own
            flipped |= (outflank - (outflank != 0)) & mask
        return flipped

    @staticmethod
    def _b64(x):
        return x & 0xFFFFFFFFFFFFFFFF

    @staticmethod
    def _flip_diag_a1h8(x):
        x=x
        k1 = 0x5500550055005500
        k2 = 0x3333000033330000
        k4 = 0x0f0f0f0f00000000
        t = k4 & (x ^ ReversiEnv._b64(x << 28))
        x ^= t ^ (t >> 28)
        t = k2 & (x ^ ReversiEnv._b64(x << 14))
        x ^= t ^ (t >> 14)
        t = k1 & (x ^ ReversiEnv._b64(x << 7))
        x ^= t ^ (t >> 7)
        return x

    @staticmethod
    def _rotate90(x):
        return ReversiEnv._flip_diag_a1h8(ReversiEnv._flip_vertical(x))

    @staticmethod
    def _flip_vertical(x):
        k1 = 0x00FF00FF00FF00FF
        k2 = 0x0000FFFF0000FFFF
        x = ((x >> 8) & k1) | ((x & k1) << 8)
        x = ((x >> 16) & k2) | ((x & k2) << 16)
        x = (x >> 32) | ReversiEnv._b64(x << 32)
        return x

    @staticmethod
    def _rotate180(x):
        return ReversiEnv._rotate90(ReversiEnv._rotate90(x))

    @staticmethod
    def _get_symmetries(own, enemy, policy, z=None):
        # returns moves in the format (own_saved, enemy_saved), list(policy_saved.reshape((64,))
        moves = []
        for flip in [False, True]:
            for rot_right in range(4):
                own_saved, enemy_saved, policy_saved = own, enemy, policy.reshape((8, 8))
                if flip:
                    own_saved = ReversiEnv._flip_vertical(own_saved)
                    enemy_saved = ReversiEnv._flip_vertical(enemy_saved)
                    policy_saved = np.flipud(policy_saved)
                if rot_right:
                    for _ in range(rot_right):
                        own_saved = ReversiEnv._rotate90(own_saved)
                        enemy_saved = ReversiEnv._rotate90(enemy_saved)
                    policy_saved = np.rot90(policy_saved, k=-rot_right)
                if z == None:
                    moves.append([(own_saved, enemy_saved), list(policy_saved.reshape((64,)))])
                else:
                    moves.append([(own_saved, enemy_saved), list(policy_saved.reshape((64,))), z])
        return moves

    def __repr__(self):
        return self._board.__repr__()
    ######################################################################
    class Board:
        def __init__(self, key=None,init_type=False):
            if key==None:
                black, white, next_player=None,None,None
            else:

                black,white,next_player = self._stateFromKey(key)

            self.black = black or (0b00001000 << 24 | 0b00010000 << 32)
            self.white = white or (0b00010000 << 24 | 0b00001000 << 32)
            self.next_player = next_player or Player.black.value
            self.height = ReversiEnv.height
            self.width = ReversiEnv.width
            if init_type:
                self.black, self.white = self.white, self.black

        def clone(self):
            st = ReversiEnv()
            st._board = self.board.clone()
            st.next_player = st._board.next_player
            st._turn = self.turn
            st.done = self.done
            st.winner = self.winner  # type: Winner
            st._resigned = self.resigned
            return st

        @property
        def planes(self):
            try:
                b = bit_to_array(self.black, self.height * self.width)
                w = bit_to_array(self.white, self.height * self.width)

                basicPlanes = np.array((b, w))
                ret = np.reshape(basicPlanes, (2, self.height, self.width))
            except:
                pass
                raise
            return ret

        @property
        def number_of_black_and_white(self):
            return bit_count(self.black), bit_count(self.white)

        @property
        def key(self):  # abbredviated string rep of the board.
            rep = [self.black, self.white, self.next_player]
            key = json.dumps(rep)
            return key
        def _stateFromKey(self,key):

            [black, white, next_player]=json.loads(key)
            return black, white, next_player

        def clone(self):
            st=ReversiEnv.Board(self.key)

            return st
        def __repr__(self):
            BLACK_CHR = "O"
            WHITE_CHR = "X"
            EXTRA_CHR = "*"
            with_edge = True
            extra = None
            """
             0  1  2  3  4  5  6  7
             8  9 10 11 12 13 14 15
            ..
            56 57 58 59 60 61 62 63

            0: Top Left, LSB
            7*6: Bottom Right

            :param black: bitboard
            :param white: bitboard
            :param with_edge:
            :param extra: bitboard
            :return:
            """
            array = [" "] * 8 * 8
            extra = extra or 0
            black = self.black
            white = self.white
            for i in range(8 * 8):
                if black & 1:
                    array[i] = BLACK_CHR
                elif white & 1:
                    array[i] = WHITE_CHR
                elif extra & 1:
                    array[i] = EXTRA_CHR
                black >>= 1
                white >>= 1
                extra >>= 1

            # ret = ""
            ret = f"{self.next_player} to play \n"
            if with_edge:
                ret = "#" * 10 + "\n"
            for y in range(8):
                if with_edge:
                    ret += "#"
                ret += "".join(array[y * 8:y * 8 + 8])
                if with_edge:
                    ret += "#"
                ret += "\n"
            if with_edge:
                ret += "#" * 10 + "\n"
            return ret

            return ret
if __name__ == '__main__':

    env=ReversiEnv()
    own = env._board.black
    en = env._board.white
    s=1

    for _ in range(60):
        env.render()
        mves=env.legal_moves_list()
        print(mves)

        m=mves[-1]
        env.step(m)

    env.render()
    print(env.legal_moves_list())
    print(env._board)
    print(env.is_terminal())
    print(env.winner)
    env.reset()
    for i in range(5):
        print("Step:0")
        env.step(0)
        env.step(4)

        print(env.legal_moves_list())
        print(env.legal_moves_mask())
        print(env.get_result())
        print(env._board)

        p=env._board.planes
        print(env._board.planes)

    print(env.legal_moves_list())
    print(env.legal_moves_mask())
    print(f"bittoarray {bit_to_array(own,env.width*env.height)}")
    print(f"bittoarray {bit_to_array(en,env.width*env.height)}")

    c=env._find_correct_moves(own, en)
    print(f"CorrectMoves:{c}")
    print(f"bittoarray {bit_to_array(c,env.width)}")


