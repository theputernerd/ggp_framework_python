import numpy as np
from env.env_inherit_from import Environment
from lib.helpers import bit_to_array,bit_count
import json
import enum
Player = enum.Enum("Player", "black white")
Winner = enum.Enum("Winner", "black white draw")


class TTT(Environment):
    width=3
    height=3
    def __init__(self, key=None):
        width, height=TTT.width,TTT.height
        n_inputs=width * height
        n_actions=n_inputs
        n_planes=2
        name="Tic Tac Toe"
        shortname=f"TTT{width}x{height}"
        super().__init__(width,height,n_inputs,n_actions,n_planes,name,shortname)

        self._actionShape=np.array([width, height])
        self._turn = 0

        self._resigned = False
        ############################## Set the board up
        self._black = 0b0
        self._white = 0b0
        if key == None:
            self.next_player = Player.black.value
            self.reset()
            pass
        else:
            self.reset() #variables might not be initialised
            self.update(key)
    def clone(self):
        st=TTT(self.key)
        st.update(self.key)
        return st

    def is_terminal(self):
        return self.done
    def get_result(self):
        if self.winner == Winner.black.value:
            return 1
        elif self.winner == Winner.white.value:
            return 2
        elif self.winner == Winner.draw.value:
            return 3
        else:
            return 0
    def reset(self):
        self._turn = 0
        self.done = False
        self.winner = None
        self._resigned = False
        self.next_player=1
        self._wList = np.zeros([self.height, self.width])
        self._bList = np.zeros([self.height, self.width])
        self._combined = []
        self._black = 0b0
        self._white = 0b0
        for i in range(self.height):
            self._combined.append([])
            for j in range(self.width):
                self._combined[i].append(' ')
        return self
    def step(self, action):
        #Action has to be an integer!!!.
        action=np.array(action)
        if action is None:
            self._resigned()
            return self.key, {}
        if self.done:
            print("Game is over. But you tried to take another move.")
            return self.key
        legalMove=False
        pTurn=self.next_player
        x,y=self._val_to_action(action)
        assert x<self._actionShape[0]
        assert y < self._actionShape[1]
        if self._combined[y][x] == ' ':
            self._combined[y][x] = ('X' if pTurn == Player.white.value else 'O')
            legalMove=True
            val=self._action_to_val(x, y)

            #################now do the bit moves
            b=1
            b=b<<val

            if pTurn==Player.white.value:
               self._white=int(np.bitwise_or(self._white, b))
            elif pTurn == Player.black.value:
               self._black=int(np.bitwise_or(self._black, b))
            else :
               assert False ##Incorrect player

        else:
            pass

        if not legalMove:
            print(action)
            assert False #illegal move
            pass  ##TODO: Make illegal move
        self._turn = bit_count(self._white) + bit_count(self._black)
        self.next_player=3-self.next_player
        self._checkTerm()
        return self.key

    def legal_moves_mask(self):
        legal = np.zeros(self._actionShape,dtype=int) #[0, 0, 0, 0, 0, 0, 0]
        legalM=np.zeros(self.width*self.height)
        for j in range(self.width):
            for i in range(self.height):
                if self._combined[i][j] == ' ':
                    legal[j][i] = 1
                    val=self._action_to_val(j, i)
                    legalM[val]=1
        return legalM
    def render(self):
        pTurn=self.next_player
        print(f"\nPly: {self._turn}. {('X' if pTurn == Player.white.value else 'O')} to play.")
        bd=""
        for i in range(self.height):
            bd+=f"\t{i}"
            for j in range(self.width):
                bd +="| " + str(self._combined[i][j]) + " "
            bd +="|\n"
        print(bd,end="")
        line1="\t"
        line2="\t"
        for j in range(self.width):
            line1+="   -"
            line2+=f"   {j}"
        print(line2)


        if self.done:
            print("Game Over!")
            if self.winner == Winner.white.value:
                print("X is the winner")
            elif self.winner == Winner.black.value:
                print("O is the winner")
            else:
                print("Game was a draw")
    def __str__(self):
        st=""
        pTurn = self.next_player
        st+= f"\nPly:{self._turn}.{('X' if pTurn == Player.white.value else 'O')} to play.\n"
        bd = ""
        for i in range(self.height):
            bd += f"\t{i}"
            for j in range(self.width):
                bd += "| " + str(self._combined[i][j]) + " "
            bd += "|\n"
        st+=bd
        line1 = "\t"
        line2 = "\t"
        for j in range(self.width):
            line1 += "   -"
            line2 += f"   {j}"
        # print(line1)
        #st += line1 + "\n"
        st+=line2+"\n"

        if self.done:
            st +="Game Over!"
            if self.winner == Winner.white.value:
                st +="X is the winner"
            elif self.winner == Winner.black.value:
                st +="O is the winner"
            else:
                st +="Game was a draw"
            st+="\n"
        return st

    @property
    def planes(self):
        #(own,en)=(self.white, self.black) if self.next_player == 1 else (self.black, self.white)
        (own,en)= self._getOwnEn()#(self.black, self.white) if self.next_player == 1 else (self.white, self.black)
        b=bit_to_array(own,self.height*self.width)
        w=bit_to_array(en,self.height*self.width)
        basicPlanes=np.array((b,w))
        ret = np.reshape(basicPlanes, (2, self.height, self.width))  # planes are simply my pieces, opponent pieces
        assert ret.shape == (self.n_planes, self.height, self.width)
        return ret
    def update(self,key): #given a key sets the board.
        [self._black, self._white, self.next_player] = json.loads(key)
        self._bList = bit_to_array(self._black, self.height * self.width)
        self._wList = bit_to_array(self._white, self.height * self.width)
        b = np.reshape(self._bList, (self.height, self.width))
        w = np.reshape(self._wList, (self.height, self.width))
        self._combined = []
        for i in range(self.height):
            self._combined.append([])
            for j in range(self.width):
                self._combined[i].append([])
                if b[i][j] == 1:
                    self._combined[i][j] = 'O'
                elif w[i][j] == 1:
                    self._combined[i][j] = 'X'
                else:
                    self._combined[i][j] = ' '
        self.done = False
        self.winner = None
        self._resigned = False
        self._turn = bit_count(self._white) + bit_count(self._black)  #self.turn_n()
        self._checkTerm()#need to now do the other stuff and check where we are at.
    @property
    def key(self):  # abbredviated string rep of the board.
        rep = [self._black, self._white, self.next_player]
        key = json.dumps(rep)
        return key

    def __repr__(self):
        BLACK_CHR = "O"
        WHITE_CHR = "X"
        EXTRA_CHR = "*"
        with_edge = True
        extra = None
        ret=""
        for i in range(self.height):
            ret+=("\n")
            for j in range(self.width):
                ret +=("| " + str(self._combined[i][j]) + ' ')
            ret +=("|")

        return ret

    def _getOwnEn(self):
        """
         Gives an integer representation of each players pieces.
        :return:
        """
        (own, en) = (self._black, self._white) if self.next_player == 1 else (self._white, self._black)
        return (own, en)

    def _checkTerm(self):
        self._check_for_three()
        if self.done: return
        if self._turn >= self.height * self.width:
            # print("!!!!!!!!!!!!!!!!!!ITS A DRAW!!!!!!!!!!!!!!!!!")
            self.done = True
            if self.winner is None:
                self.winner = Winner.draw.value

    @staticmethod
    def _action_to_val(x, y): #converts a action into a single int for a flattened action array
        val=y * TTT.width + x
        return val
    @staticmethod
    def _val_to_action(val):
        row=val%TTT.width
        col=int(val/TTT.width)
        return row,col
    def _reverse_bit(self, num):
        result = 0
        while num:
            result = (result << 1) + (num & 1)
            num >>= 1
        return result
    def _check_for_three(self):
        for i in range(self.height):
            for j in range(self.width):
                if self._combined[i][j] != ' ':
                    # check if a vertical four-in-a-row starts at (i, j)
                    if self._vertical_check(i, j):
                        self.done = True
                        return

                    # check if a horizontal four-in-a-row starts at (i, j)
                    if self._horizontal_check(i, j):
                        self.done = True
                        return

                    # check if a diagonal (either way) four-in-a-row starts at (i, j)
                    diag_fours = self._diagonal_check(i, j)
                    if diag_fours:
                        self.done = True
                        return
    def _vertical_check(self, row, col):
        # print("checking vert")
        three_in_a_row = False
        consecutive_count = 0

        for i in range(row, self.height):
            if self._combined[i][col].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= 3:
            three_in_a_row = True
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        return three_in_a_row
    def _horizontal_check(self, row, col):
        three_in_a_row = False
        consecutive_count = 0

        for j in range(col, self.width):
            if self._combined[row][j].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= 3:
            three_in_a_row = True
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        return three_in_a_row
    def _diagonal_check(self, row, col):
        three_in_a_row = False
        count = 0

        consecutive_count = 0
        j = col
        for i in range(row, self.height): #####Check
            if j > self.width-1:
                break
            elif self._combined[i][j].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= 3:
            count += 1
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        consecutive_count = 0
        j = col
        for i in range(row, -1, -1):
            if j > self.width-1:
                break
            elif self._combined[i][j].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= 3:
            count += 1
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        if count > 0:
            three_in_a_row = True

        return three_in_a_row
    def _resigned(self):
        if self.next_player == Player.white.value:
            self.winner = Winner.white.value
        else:
            self.winner = Winner.black.value
        self.done = True
        self._resigned = True
    def _get_symmetries(self, planes, policy, z=None):
        # returns sym_planes in the format (own_saved, enemy_saved), list(policy_saved.reshape((64,))
        sym_planes = []
        planes, policy_saved = planes[0].tolist(), policy.reshape(self._actionShape)

        if z == None:
            sym_planes.append([planes, list(policy_saved.reshape((self.n_actions,)))])
        else:
            sym_planes.append([planes, list(policy_saved.reshape((self.n_actions,))), z])
            #add the inverse perspective.
            invPolicy=1-policy_saved

        return sym_planes


from player.player_human import Human_Player

if __name__ == '__main__':

    env=TTT()
    own = env._black
    en = env._white
    s=1
    print(env)
    env2=env.clone()
    v=env2._action_to_val(1, 1)
    env2.step(0)
    env2.render()
    env.render()
    move=env._val_to_action(v)

    print(env)
    print(env.key)
    #env.next_player=2
    #env.board.next_player=2
    print(env.legal_moves_mask())
    print(env.legal_moves_list())
    env.render()

    #m=env.action_to_val((2,0))
    env.step(env._action_to_val(2, 0))
    print(env.legal_moves_list())
    env.render()
    val=env._action_to_val(1, 1) #if you wanted to give it x and y coordinates
    env.step(val)
    print(env.legal_moves_list())
    env.render()

    env.step(env._action_to_val(1, 0))
    print(env.legal_moves_list())
    env.render()

    env.step(env._action_to_val(2, 2))
    print(env.legal_moves_list())
    env.render()

    env.step(env._action_to_val(2, 1))
    print(env.legal_moves_list())
    env.step(env._action_to_val(1, 2))
    print(env.legal_moves_list())
    env.render()
    env.step(env._action_to_val(0, 2))
    #env.step(env.action_to_val(0, 0))
    print(env.legal_moves_list())
    env.step(3)
    print(env.legal_moves_list())
    env.step(0)
    print(env)
    env.render()
    print(env.is_terminal())
    print(env.winner)



