import enum
import numpy as np
from env.env_inherit_from import Environment
from lib.helpers import bit_to_array,bit_count
import json

Player = enum.Enum("Player", "black white")
Winner = enum.Enum("Winner", "black white draw")

def getOwnEn(b):
    (own, en) = (b.black, b.white) if b.next_player == 1 else (b.white, b.black)
    return (own, en)
class Connect4Env(Environment):

    def __init__(self, key=None,width=7,height=6):
        """
        Note that you can have a different size board by setting width and height. It's still connect4 though.
        :param key: Key to set the board to
        :param width: Width of the board
        :param height: Height of the board
        """
        n_inputs = width * height
        n_actions = width
        n_planes = 2
        name = f"Connect4_{width}x{height}"
        shortname = f"C4{width}x{height}"
        super().__init__(width, height, n_inputs, n_actions, n_planes, name, shortname)
        self._actionShape=(width, 1) #used to get a map if the legal moves
        self._turn = 0
        ############################## Set the board up
        self._black = 0b0  # must be an int representing a binary representation of the board
        self._white = 0b0
        self.reset()
        if key == None: #if no key then create a new game
            self.next_player = Player.black.value
        else:
            self.update(key)

    def clone(self):
        st=Connect4Env(key=self.key,width=self.width,height=self.height)
        st.update(self.key)
        return st

    def get_result(self):
        if self.winner == Winner.black.value:
            return 1
        elif self.winner == Winner.white.value:
            return 2
        elif self.winner == Winner.draw.value:
            return 3
        else:
            return 0
    def is_terminal(self):
        return self.done
    def reset(self):
        self._turn = 0
        self.done = False
        self.winner = None
        self.next_player = 1
        self._wList = np.zeros([self.height, self.width])
        self._bList = np.zeros([self.height, self.width])
        self._combined = []
        self._black = 0b0
        self._white = 0b0

        #create the empty array
        for i in range(self.height):
            self._combined.append([])
            for j in range(self.width):
                self._combined[i].append(' ')

    def update(self, key):
        self._setBoard(key)
        self._bList = bit_to_array(self._black, self.height * self.width)
        self._wList = bit_to_array(self._white, self.height * self.width)
        b = np.reshape(self._bList, (self.height, self.width))
        w = np.reshape(self._wList, (self.height, self.width))
        for i in range(self.height):
            self._combined.append([])
            for j in range(self.width):
                self._combined[i].append([])
                if b[i][j] == 1:
                    self._combined[i][j] = 'O'
                elif w[i][j] == 1:
                    self._combined[i][j] = 'X'
                else:
                    self._combined[i][j] = ' '
        self.done = False
        self.winner = None
        self._turn = bit_count(self._white) + bit_count(self._black)
        self._checkTerm()
        return self.key

    def step(self, action):
        if action is None:
            self._resigned()
            return self.key
        legalMove = False
        if self.done:
            print("Game is over. But you tried to take another move.")
            return self.key
        pTurn = self.next_player

        for i in range(self.height - 1, -1, -1):
            if self._combined[i][action] == ' ':
                self._combined[i][action] = ('X' if pTurn == Player.white.value else 'O')
                legalMove = True
                val = (i) * self.width + action
                #################now do the bit moves
                b = 1
                try:
                    b = b << val
                except:
                    pass
                if pTurn == Player.white.value:
                    self._white = int(np.bitwise_or(self._white, b))
                elif pTurn == Player.black.value:
                    self._black = int(np.bitwise_or(self._black, b))
                else:
                    assert False  ##Incorrect player

                break
            else:
                pass

        if not legalMove:
            self.render()
            assert False  # illegal move
            pass  ##TODO: Make illegal move
        self._turn = np.sum(self._number_of_black_and_white)
        self.next_player = 3 - self.next_player
        self.next_player = self.next_player
        self._checkTerm()

        return self.key

    def render(self):
        pTurn=self.next_player
        print(f"Round: {self._turn}. {('X' if pTurn == Player.white.value else 'O')} to play.")
        bd = ""
        for i in range(self.height):
            bd += f"\t{i}"
            for j in range(self.width):
                bd += "| " + str(self._combined[i][j]) + " "
            bd += "|\n"
        print(bd, end="")
        line1 = "\t"
        line2 = "\t"
        for j in range(self.width):
            line1 += "   -"
            line2 += f"   {j}"
        # print(line1)
        print(line2)

        if self.done:
            print("Game Over!")
            if self.winner == Winner.white.value:
                print("X is the winner")
            elif self.winner == Winner.black.value:
                print("O is the winner")
            else:
                print("Game was a draw")

    def legal_moves_mask(self):
        legal = np.zeros(self.width) #[0, 0, 0, 0, 0, 0, 0]
        for j in range(self.width):
            for i in range(self.height):
                if self._combined[i][j] == ' ':
                    legal[j] = 1
                    break

        return legal

    @property
    def key(self):  # abbredviated string rep of the board.
        rep = [self._black, self._white, self.next_player]
        key = json.dumps(rep)
        return key

    @property
    def planes(self):
        (own, en) = (self._black, self._white) if self.next_player == 1 \
            else (self._white, self._black)
        b = bit_to_array(own, self.height * self.width)
        w = bit_to_array(en, self.height * self.width)
        basicPlanes = np.array((b, w))
        ret = np.reshape(basicPlanes, (2, self.height, self.width))
        assert ret.shape == (self.n_planes, self.height, self.width)
        return ret
    def __repr__(self):
        BLACK_CHR = "O"
        WHITE_CHR = "X"
        EXTRA_CHR = "*"
        with_edge = True
        extra = None
        ret=""
        for i in range(self.height):
            ret+=("\n")
            for j in range(self.width):
                ret +=("| " + str(self._combined[i][j]) + ' ')
            ret +=("|")

        return ret

    def _setBoard(self, key):  # given a key sets the board.
        """
        inverse of key - given a key sets the board.
        :param key:
        :return: None
        """
        [self._black, self._white, self.next_player] = json.loads(key)

    @property
    def _turn_n(self): #how many ply have taken place
        return self._turn

    def _checkTerm(self):
        self._check_for_fours()
        if self.done: return
        if self._turn >= self.height * self.width:
            # print("!!!!!!!!!!!!!!!!!!ITS A DRAW!!!!!!!!!!!!!!!!!")
            self.done = True
            if self.winner is None:
                self.winner = Winner.draw.value



    def _reverse_bit(self, num):
        result = 0
        while num:
            result = (result << 1) + (num & 1)
            num >>= 1
        return result

    def _check_for_fours(self):
        for i in range(self.height):
            for j in range(self.width):
                if self._combined[i][j] != ' ':
                    # check if a vertical four-in-a-row starts at (i, j)
                    if self._vertical_check(i, j):
                        self.done = True
                        return

                    # check if a horizontal four-in-a-row starts at (i, j)
                    if self._horizontal_check(i, j):
                        self.done = True
                        return

                    # check if a diagonal (either way) four-in-a-row starts at (i, j)
                    diag_fours = self._diagonal_check(i, j)
                    if diag_fours:
                        self.done = True
                        return

    def _vertical_check(self, row, col):
        # print("checking vert")
        four_in_a_row = False
        consecutive_count = 0

        for i in range(row, self.height):
            if self._combined[i][col].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= 4:
            four_in_a_row = True
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        return four_in_a_row

    def _horizontal_check(self, row, col):
        four_in_a_row = False
        consecutive_count = 0

        for j in range(col, self.width):
            if self._combined[row][j].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= 4:
            four_in_a_row = True
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        return four_in_a_row

    def _diagonal_check(self, row, col):
        four_in_a_row = False
        count = 0

        consecutive_count = 0
        j = col
        for i in range(row, self.height): #####Check
            if j > self.width-1:
                break
            elif self._combined[i][j].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= 4:
            count += 1
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        consecutive_count = 0
        j = col
        for i in range(row, -1, -1):
            if j > self.width-1:
                break
            elif self._combined[i][j].lower() == self._combined[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= 4:
            count += 1
            if 'x' == self._combined[row][col].lower():
                self.winner = Winner.white.value
            else:
                self.winner = Winner.black.value

        if count > 0:
            four_in_a_row = True

        return four_in_a_row

    def _resigned(self):
        if self.next_player == Player.white.value:
            self.winner = Winner.white.value
        else:
            self.winner = Winner.black.value
        self.done = True


    @staticmethod
    def _action_to_val(val):  # converts a action into a single int for a flattened action array
        return val

    @staticmethod
    def _val_to_action(val):
        return val

    def _get_symmetries(self, planes, policy, z=None):
        # returns moves in the format (own_saved, enemy_saved), list(policy_saved.reshape((64,))
        moves = []
        planes, policy_saved = planes[0].tolist(), policy.reshape(self._actionShape)

        if z == None:
            moves.append([planes, list(policy_saved.reshape((self.n_actions,)))])
        else:
            moves.append([planes, list(policy_saved.reshape((self.n_actions,))), z])
            #add the inverse perspective.
            invPolicy=1-policy_saved
            invPolicy=invPolicy/np.sum(invPolicy)
            #b=self.Board(own_saved,enemy_saved)

            #enBoard=self.Board(enemy_saved,own_saved)
            #moves.append([(enemy_saved, own_saved), list(invPolicy.reshape((self.n_actions,))), -z])

        return moves


    @property
    def _number_of_black_and_white(self):
        return bit_count(self._black), bit_count(self._white)



if __name__ == '__main__':

    #env=Connect4_5Env()
    env_ptr=Connect4Env
    env=env_ptr(width=5,height=5)
    env.step(0)
    env.render()

    s=1
    env.step(0+s)
    env.step(0)
    env.step(1+s)
    env.step(1+s)
    env.step(1+s)
    env.step(2+s)
    env.step(2+s)
    env.step(2+s)
    env.step(3+s)
    env.step(3+s)
    env.step(2+s)
    env.step(3+s)
    env.step(3+s)
    env.step(0)
    env.step(3+s)
    env.step(0)
    env.step(1)
    env.render()
    print(env)
    print(env.is_terminal())
    print(env.winner)
    env.reset()
    while not env.done:
        print("Step:0")
        env.step(0)
        env.step(4)

        print(env.legal_moves_mask())
        print(env.legal_moves_list())
        print(env.get_result())
        env.render()
        planes = env.planes
        key=env.key

        env2 = env_ptr()
        env2.update(key)

        print(env_ptr(env.key))
        p=env.planes
        print(p)

    print(env.legal_moves_mask())
    print(env.legal_moves_list())

    #c=env.find_correct_moves(own,en)
    #print(f"CorrectMoves:{c}")
    #print(f"bittoarray {bit_to_array(c,env.width)}")


