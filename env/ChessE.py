import enum
import numpy as np
from env.env_inherit_from import Environment
from lib.helpers import *
from logging import getLogger
import chess.pgn
import chess.variant
import random
import json
logger = getLogger(__name__)
###TO use this game you need to install python chess.

# noinspection SpellCheckingInspection
pieces_order = 'KQRBNPkqrbnp' # 12x8x8
castling_order = 'KQkq'       # 4x8x8
# fifty-move-rule             # 1x8x8
# en en_passant               # 1x8x8

ind = {pieces_order[i]: i for i in range(12)}

class Chess(Environment):
    width=8
    height=8
    pieceRank=6 #this is used to move pieces forwards if you want a simpler game.
    full=True #if none then it is only half board with pieces on pieceRank
    #variant=chess.variant.SuicideBoard
    variant=chess.variant.RacingKingsBoard
    def __init__(self,width=None,height=None,cmd=0): #cmd is required by environment. 1 is selfplay. 3 is eval. 4 is competition. Do with these what you will
        if width!=None:
            Chess.width=width
        if height != None:
            Chess.height = height

        n_cells = Chess.width * Chess.height

        if Chess.pieceRank != None:
            prank=f"{Chess.pieceRank}"
            name = f"ChessR_RK95_{Chess.width}x{Chess.height}_pr{prank}"
            shortname=f"CHRR_K95{Chess.width}x{Chess.height}_pr{prank}"
        elif Chess.full:
            name = f"ChessR_RK95_{Chess.width}x{Chess.height}_full"
            shortname = f"CHRR_K95{Chess.width}x{Chess.height}_full"

        else:
            name = f"ChessR_RK95_{Chess.width}x{Chess.height}"
            shortname = f"CHRR_K95{Chess.width}x{Chess.height}"
        n_planes = 14
        n_actions = n_cells*n_cells+45 #4140 is largest. 45 is for additional promotion moves to Queen.
        super().__init__(Chess.width, Chess.height,n_inputs=n_cells, n_actions=n_actions, n_planes=n_planes, name=name,shortname=shortname)
        self.actionShape=(self.n_actions,1)

        self.board = self.Board()
        self.turn = 0
        self.winner = None  # type: Winner
        self.resigned = False
        self.next_player = self.board.next_player
        self.num_halfmoves = 0
        self.done=False

    #def done(self):
    #    return self.winner is not None

    @property
    def key(self): #abbredviated string rep of the board.
        return self.board.key
    def setenv(self,key):
        #fen=json.loads(key)
        self.update(key)

    def clone(self):
        st=Chess()
        #ms=self.board.API.move_stack #remove
        st.update(self.key)
        st.winner = self.winner # type: Winner
        st.done=self.done
        st.resigned = self.resigned
        return st
    def is_terminal(self):
        return self.done
    def get_result(self):
        ###JOE CHANGED. TODO CHECK.
        if self.winner==None: return 0
        self.done=True if self.winner!=None else False
        return self.winner

    def reset(self):
        self.board = self.Board()
        self.num_halfmoves = 0
        self.turn = 0
        self.winner = None
        self.resigned = False
        self.next_player = self.board.next_player
        self.done=False
        return self

    def update(self, key,fromsearch=False):
        self.board=self.Board(key)
        self.winner=None
        self.resigned = False

        self.next_player = self.board.next_player
        #self.check_game_over(fromsearch=fromsearch) #check if game over and sets winner and resigned
        self._game_over(fromsearch)
        #if self.board.API.is_game_over(claim_draw=fromsearch):
        #    self._game_over(fromsearch=fromsearch)
        #else:
        #    self.winner = None
        #    self.resigned = False

        return self
    @property
    def turn_n(self): #how many ply have taken place
        return self.turn

    def convertActionToMove(self,val):
        promoteTo=None #
        """            fromSquare = m.from_square
                from_r = chess.square_rank(fromSquare)
                from_f = chess.square_file(fromSquare)
                to_f = chess.square_file(m.to_square)
                delta=(to_f - from_f)+1 #aways adding, I cant reverse this if there is a negative. Causes a few more actions to be required in the NN
                raw_val = from_f * 3 + delta
                if from_r==1: offset=64*64 #square rank and files start at 0
                elif from_r==6: offset=64*64+22
                else: assert False #only promote from rank 2 or 7
                val=raw_val+offset
                opPlane[val] = 1 #Queen promotions.
        """
        if val>64*64: #then it is a special move.
            if val<=64*64+22: #then it is a promotion to Q move fom rank 2. largest val is ....
                offset=64*64
                pickr=1
                placer=0
            elif val<=64*64+22+22 :#largest Qpromotion move for rank 7 is 4140
                offset=64*64+22
                pickr=6
                placer=7
            else:
                assert False #no other promotion moves are implemented.
            val=val-offset
            #raw_val = from_f * 3 + (to_f - from_f)
            pickf=int(val/3)
            delta=int(val%3)-1
            placef=pickf+delta
            pick=pickr*8+pickf
            place=placer*8+placef
            promoteTo=5
        else:
            pick=int(val%64)
            place = int(val / 64)

        m=chess.Move(pick,place,promoteTo)
        action=m.uci()

        return action

    @property
    def white_won(self): #white is player 1
        return self.winner == 1


    def step(self, action: int,check_over = True,fromsearch=False):
        action=self.convertActionToMove(action)
        if check_over and action is None: #resign is an action.
            self.next_player = 1 if self.board.API.turn == chess.WHITE else 2
            self.board.next_player = self.next_player
            self._resign()
            return
        try:
            self.board.API.push_uci(action)
        except:
            self.render()
            print(self.legal_moves_list())
            print(self.board.API.move_stack)

            raise
        # turns off draw by repetitions.

        #self.board.API.halfmove_clock=0 #turns off draw by repetitions.
        #self.board.API.clear_stack()
        ###

        self.num_halfmoves += 1
        nPly = len(self.board.API.stack)
        if not fromsearch:
            self.board.API.move_stack=[] #dont allow game over because of repetition. Want it in search to discourage repeated moves.

        if check_over and self.board.API.result(claim_draw=fromsearch) != "*":
            self._game_over(fromsearch=fromsearch) #TODO: put the if inside the function. Doubles up on checking the state.

        elif nPly > 300:
            pass
            #print(f"________________ >{nPly} moves. not stopping")
            #self._game_over()

        self.next_player = 1 if self.board.API.turn == chess.WHITE else 2
        self.board.next_player=self.next_player
        #if fromChess!=self.next_player:
        #    assert False
        return self.board, {}

    def _game_over(self,fromsearch=False):
        if self.board.API.is_game_over(claim_draw=fromsearch):
            self.done = True

            if self.winner is None:
                self.result = self.board.API.result(
                    claim_draw=fromsearch)  # FEN doesnt include 3fold repetition or 50 moves.
                if self.result == '1-0':
                    self.winner = 1  # player 1 is first.
                elif self.result == '0-1':
                    self.winner = 2
                else:
                    self.winner = 3
        else:
            self.winner = None
            self.resigned = False


    def _resign(self):
        self.resigned = True
        assert False #make sure that next_player is incremented. resign is a move.
        winner=self.board.next_player
        self.result=winner



    def legal_moves(self):

        legal=self.get_legal_moves_mask(self.board)

        return legal
    def convertMoveToAction(self,m):
        u=m.uci()
        if len(m.uci()) == 5:
            # then there's a promotion Opp. Only promote to n or q
            # val=64+
            fromSquare = m.from_square
            from_r = chess.square_rank(fromSquare)
            from_f = chess.square_file(fromSquare)
            to_f = chess.square_file(m.to_square)
            delta = (
                                to_f - from_f) + 1  # aways adding, I cant reverse this if there is a negative. Causes a few more actions to be required in the NN
            raw_val = from_f * 3 + delta
            if from_r == 1:
                offset = 64 * 64  # square rank and files start at 0
            elif from_r == 6:
                offset = 64 * 64 + 22
            else:
                assert False  # only promote from rank 2 or 7
            val = raw_val + offset
            ###TO DO Promotion to other pieces just add anoter +22 offset and repeat
            # dont forget to increase the output size of the NN.

        else:
            val = m.from_square + 64 * m.to_square
        return val

    def legal_moves_mask(self):
        opPlane = np.zeros(self.n_actions)
        l = self.board.API.legal_moves

        for m in l:

            u=m.uci()
            val=self.convertMoveToAction(m)
            opPlane[val] = 1
        return opPlane

    def legal_moves_list(self):
        mask = self.legal_moves_mask()
        idx = np.where(mask)[0]

        return idx
    def _resigned(self):
        #Ensure that nextplayer is incremented after before this is called.
        self.winner=3-self.next_player
        self.done = True

        self.resigned = True

    def render(self):
        pTurn=self.next_player
        print(f"\nRound: {self.board.API.halfmove_clock}. {('UC' if pTurn == 1 else 'lc')} to play.")
        print(self.board)
        print(self.board.API.fen())

        if self.done:
            print("Game Over!")
            print(self.get_how_game_over_str())

            if self.winner == 1:
                print("UC is the winner")
            elif self.winner == 2:
                print("lc is the winner")
            else:
                print("Game was a draw")
    def get_how_game_over_str(self):
        str=""
        str += "is_stalemate. " if self.board.API.is_stalemate() else ""
        str +="is_checkmate. " if self.board.API.is_checkmate() else ""
        str += "is_insufficient_material. " if self.board.API.is_insufficient_material() else ""
        str += "is_seventyfive_moves. " if self.board.API.is_seventyfive_moves() else ""
        str += "is_fivefold_repetition. " if self.board.API.is_fivefold_repetition() else ""
        str += "can_claim_fifty_moves. " if self.board.API.can_claim_fifty_moves() else ""
        str += "can_claim_threefold_repetition. " if self.board.API.can_claim_threefold_repetition() else ""

        return str
    @property
    def observation(self):
        """

         :rtype: Board
         """
        return self.board
        #return ''.join(''.join(x for x in y) for y in self.board)

    def deltamove(self, fen_next):
        moves = list(self.board.API.legal_moves)
        for mov in moves:
            self.board.API.push(mov)
            fee = self.board.API.fen()
            self.board.API.pop()
            if fee == fen_next:
                return mov.uci()
        return None

    def replace_tags(self):
        pass
        return replace_tags_board(self.board.API.fen())
    @property
    def canon_planes(self): #next_player is first plane
        return canon_input_planes(self.board.API.fen())

    @property
    def planes(self):
        return canon_input_planes(self.board.API.fen())

    def testeval(self, absolute=False) -> float:
        return testeval(self.board.API.fen(), absolute)


    @staticmethod
    def action_to_val(val):  # converts a action into a single int for a flattened action array

        return val

    @staticmethod
    def val_to_action(val):

        return val

    def get_symmetries(self,planes, policy, z=None):
        # returns moves in the format (own_saved, enemy_saved), list(policy_saved.reshape((64,))
        moves = []
        planes, policy_saved = planes.tolist(), policy.reshape(self.actionShape)

        if z == None:
            moves.append([planes, list(policy_saved.reshape((self.n_actions,)))])
        else:
            moves.append([planes, list(policy_saved.reshape((self.n_actions,))), z])
            # add the inverse perspective.
            invPolicy = 1 - policy_saved
            invPolicy = invPolicy / np.sum(invPolicy)
            # b=self.Board(own_saved,enemy_saved)

            # enBoard=self.Board(enemy_saved,own_saved)
            # moves.append([(enemy_saved, own_saved), list(invPolicy.reshape((self.n_actions,))), -z])

        return moves

    def __str__(self):
        pTurn = self.next_player
        s=""
        s+=f"\nPly: {self.board.API.halfmove_clock}. {('UC' if pTurn == 1 else 'lc')} to play.\n"
        s+=repr(self.board)+"\n"
        s+=self.board.API.fen()+"\n"

        if self.done:
            s+="Game Over!\n"
            s+=self.get_how_game_over_str()+"\n"

            if self.winner == 1:
                s+="UC is the winner\n"
            elif self.winner == 2:
                s+="lc is the winner\n"
            else:
                s+="Game was a draw\n"
        return s
    def __repr__(self):
        return self.board.__repr__()

    class Board:
        def __init__(self, key=None): #key is fen and movestack.
            #given black and white pieces reconstruct the board.
            self.boardtypePtr=Chess.variant
            self.height=Chess.height
            self.width=Chess.width
            if key==None:
                self.reset()
            else:
                try:
                    fen, mstack, sstack = self.convertfromjsonState(key)
                except:
                    print(key)
                    raise
                self.API=self.boardtypePtr(fen)
                self.API.move_stack=mstack
                self.API.stack=sstack

            self.next_player = 1 if self.API.turn == chess.WHITE else 2


        @property
        def fen(self):
            return self.API.fen()
        @property
        def epd(self):
            return self.API.epd()
        @property
        def planes(self):
            ret=canon_input_planes(self.fen)
            return ret
        def reset(self):
            fen=None
            if not Chess.full:
                if Chess.pieceRank != None:
                    krank = Chess.pieceRank  # this distance from 8th rank
                    piecesRank = Chess.pieceRank
                else:
                    krank=random.randint(2,6) #this distance from 8th rank
                    piecesRank=random.randint(krank,6)
                if krank+5==piecesRank:#bishop causes check if 3 deeper than the king.
                    piecesRank=piecesRank-1

                s="8"
                for i in range(1,7):
                    if i==krank:
                        #put king here.
                        if piecesRank==krank: #all pieces on same rank
                            #s+='/krbnNBRK/qrbnNBRQ'
                            s+='/krbnNBRK/8'

                        else:
                            s+='/k6K'
                    elif piecesRank==i:

                        #s += '/1rbnNBR1/qrbnNBRQ'
                        s += '/1rbnNBR1/8'

                    else:
                        s+="/8"
                s+=" w - - 0 1"
                #key="8/8/8/krbn4/8/8/4NBRK/8 w - - 0 1"
                fen=s
                ##TODO:Remove indent above here------------------------------------------------------

                key=self.converttojsonState(fen,[],[])

                try:
                    fen,mstack,sstack=self.convertfromjsonState(key)

                    #stack = json.loads(list(mm))
                except:
                    print(key)
                    raise
            fen="" if fen==None else fen
            self.API = self.boardtypePtr()
            #self.API.move_stack = mstack
            #self.API.stack = sstack

            self.next_player = 1 if self.API.turn == chess.WHITE else 2

            #self.next_player=1 #if self.API.turn == chess.WHITE else 1

        def getFullBoardState(self, _BoardState:chess._BoardState):
            fullboardstate = [_BoardState.halfmove_clock, _BoardState.turn,
                              _BoardState.bishops, _BoardState.castling_rights, _BoardState.ep_square,
                              _BoardState.fullmove_number, _BoardState.kings, _BoardState.knights,
                              _BoardState.occupied, _BoardState.occupied_b, _BoardState.occupied_w,
                              _BoardState.pawns, _BoardState.promoted, _BoardState.queens,
                              _BoardState.rooks]
            return fullboardstate

        def setFullBoardState(self, statelist):
            board=self.boardtypePtr()
            b=chess._BoardState(board)
            [b.halfmove_clock, b.turn,
             b.bishops, b.castling_rights, b.ep_square,
             b.fullmove_number, b.kings, b.knights,
             b.occupied, b.occupied_b, b.occupied_w,
             b.pawns, b.promoted, b.queens,
             b.rooks] = statelist
            return b

        def convertfromjsonState(self,jsonState): #returns fen and stack from a json string
            fen, mstack,sstack=json.loads(jsonState)

            u = json.loads(mstack)
            mstack = [chess.Move.from_uci(m) for m in u]
            ss = json.loads(sstack)
            sstack = [self.setFullBoardState(json.loads(bs)) for bs in ss]

            return fen,mstack,sstack


        def converttojsonState(self,fen,movestack,stack): #returns a json string from the state
            u = [m.uci() for m in movestack]
            mstack = json.dumps(u)


            s = [json.dumps(self.getFullBoardState(b)) for b in stack]
            sstack = json.dumps(s)
            return json.dumps([fen,mstack,sstack])
        def clone(self):
            k=self.key
            st=Chess.Board(k)
            #st.API=self.boardtypePtr(k)
            st.API.move_stack=list(self.API.move_stack)
            st.API.stack=list(self.API.stack)
            return st

        @property
        def key(self):  # abbredviated string rep of the board.
            rep=self.API.fen()
            key=self.converttojsonState(rep,self.API.move_stack,self.API.stack)

            return key
        def __repr__(self):

            ret=str(self.API)
            return ret



def check_current_planes(realfen, planes):
    cur = planes[0:12]
    assert cur.shape == (12, 8, 8)
    fakefen = ["1"] * 64
    for i in range(12):
        for rank in range(8):
            for file in range(8):
                if cur[i][rank][file] == 1:
                    assert fakefen[rank * 8 + file] == '1'
                    fakefen[rank * 8 + file] = pieces_order[i]

    castling = planes[12:16]
    fiftymove = planes[16][0][0]
    ep = planes[17]

    castlingstring = ""
    for i in range(4):
        if castling[i][0][0] == 1:
            castlingstring += castling_order[i]

    if len(castlingstring) == 0:
        castlingstring = '-'

    epstr = "-"
    for rank in range(8):
        for file in range(8):
            if ep[rank][file] == 1:
                epstr = coord_to_alg((rank, file))

    realfen = maybe_flip_fen(realfen, flip=is_black_turn(realfen))
    realparts = realfen.split(' ')
    assert realparts[1] == 'w'
    assert realparts[2] == castlingstring
    assert realparts[3] == epstr
    assert int(realparts[4]) == fiftymove
    # realparts[5] is the fifty-move clock, discard that
    return "".join(fakefen) == replace_tags_board(realfen)


def canon_input_planes(fen):
    fen = maybe_flip_fen(fen, is_black_turn(fen))
    return all_input_planes(fen)


def all_input_planes(fen):
    current_aux_planes = aux_planes(fen)

    history_both = to_planes(fen)
    if len(current_aux_planes)>0: #==[]:#depreciated. Should always build the planes like this>

        ret = np.vstack((history_both, current_aux_planes))
    else:
        print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&& line 619 ChessE.py. planes not what I thought. &&&&&&&&&&&&&&&&&&&&&&&&&&&&")
        ret=history_both

    #assert ret.shape == (18, 8, 8)
    return ret


def maybe_flip_fen(fen, flip = False):
    if not flip:
        return fen
    foo = fen.split(' ')
    rows = foo[0].split('/')
    def swapcase(a):
        if a.isalpha():
            return a.lower() if a.isupper() else a.upper()
        return a
    def swapall(aa):
        return "".join([swapcase(a) for a in aa])
    return "/".join([swapall(row) for row in reversed(rows)]) \
        + " " + ('w' if foo[1] == 'b' else 'b') \
        + " " + "".join(sorted(swapall(foo[2]))) \
        + " " + foo[3] + " " + foo[4] + " " + foo[5]


def aux_planes(fen):
    foo = fen.split(' ')
    #return []
    # TODO: Remove this if you want it to be without human features.

    en_passant = np.zeros((8, 8), dtype=np.float32) #all planes are 8x8
    #if foo[3] != '-':
    #    eps = alg_to_coord(foo[3])
    #    en_passant[eps[0]][eps[1]] = 1
    #fifty_move = np.zeros((8, 8), dtype=np.float32) #all planes are 8x8

    fifty_move_count = int(foo[4])
    fifty_move = np.full((8, 8), fifty_move_count, dtype=np.float32)

    castling = int(foo[4])
    #castling = foo[2]
    auxiliary_planes=[fifty_move,
                        en_passant]
    #auxiliary_planes = [np.full((8, 8), int('K' in castling), dtype=np.float32),
    #                    np.full((8, 8), int('Q' in castling), dtype=np.float32),
    #                    np.full((8, 8), int('k' in castling), dtype=np.float32),
    #                    np.full((8, 8), int('q' in castling), dtype=np.float32),
    #                    fifty_move,
    #                    en_passant]

    ret = np.asarray(auxiliary_planes, dtype=np.float32)
    #assert ret.shape == (6, 8, 8)
    assert ret.shape == (2, 8, 8)

    return ret

# FEN board is like this:
# a8 b8 .. h8
# a7 b7 .. h7
# .. .. .. ..
# a1 b1 .. h1
#
# FEN string is like this:
#  0  1 ..  7
#  8  9 .. 15
# .. .. .. ..
# 56 57 .. 63

# my planes are like this:
# 00 01 .. 07
# 10 11 .. 17
# .. .. .. ..
# 70 71 .. 77
#


def alg_to_coord(alg):
    rank = 8 - int(alg[1])        # 0-7
    file = ord(alg[0]) - ord('a') # 0-7
    return rank, file


def coord_to_alg(coord):
    letter = chr(ord('a') + coord[1])
    number = str(8 - coord[0])
    return letter + number


def to_planes(fen): #basic planes for the board.  6 pieces each.
    board_state = replace_tags_board(fen)
    pieces_both = np.zeros(shape=(12, 8, 8), dtype=np.float32)
    for rank in range(8):
        for file in range(8):
            v = board_state[rank * 8 + file]
            if v.isalpha():
                pieces_both[ind[v]][rank][file] = 1
    assert pieces_both.shape == (12, 8, 8)
    return pieces_both


def replace_tags_board(board_san):
    board_san = board_san.split(" ")[0]
    board_san = board_san.replace("2", "11")
    board_san = board_san.replace("3", "111")
    board_san = board_san.replace("4", "1111")
    board_san = board_san.replace("5", "11111")
    board_san = board_san.replace("6", "111111")
    board_san = board_san.replace("7", "1111111")
    board_san = board_san.replace("8", "11111111")
    return board_san.replace("/", "")


def is_black_turn(fen):
    return fen.split(" ")[1] == 'b'


def testeval(fen, absolute = False) -> float:
    piece_vals = {'K': 3, 'Q': 14, 'R': 5, 'B': 3.25, 'N': 3, 'P': 1} # somehow it doesn't know how to keep its queen
    ans = 0.0
    tot = 0
    for c in fen.split(' ')[0]:
        if not c.isalpha():
            continue

        if c.isupper():
            ans += piece_vals[c]
            tot += piece_vals[c]
        else:
            ans -= piece_vals[c.upper()]
            tot += piece_vals[c.upper()]
    v = ans/tot
    if not absolute and is_black_turn(fen):
        v = -v
    assert abs(v) < 1
    return np.tanh(v * 3) # arbitrary


if __name__ == '__main__':
    #Chess.pieceRank=1
    env=Chess()

    #env.update(env.Board("1nbqkbr1/Pppppp1p/7P/7P/8/8/pPPPP2p/1NBQK1R1 w KQkq - 0 1"))
    #f="1nbqkbr1/Pppppp1p/7P/7P/8/8/pPPPP2p/1NBQK1R1 w KQkq - 0"
    #pieces=f.split(' ')
    #rowsP=pieces[0].split('/')
    #r=range(0,7)
    #pos=[]
    #Ppieces = "kKqQbBrRnNbBrRnN"
    #Ppieces = list(Ppieces)
    #rndPos=np.random.choice(64-8,len(Ppieces),replace=False)+8
    #k1pos=rndPos[0]
    #while k1pos in rndPos:
    #    k1pos=np.random.choice(8*3,1,replace=False)+8*5 #k on rank 5,6,7
    #rndPos[0]=k1pos
    #k2pos=rndPos[1]

    #while k2pos in rndPos:
    #    k2pos=np.random.choice(8*3,1,replace=False)+8*5 #k on rank 5,6,7
    #rndPos[1]=k2pos
    #pieceZ=list(zip(Ppieces,rndPos))
    #pieceZ=sorted(pieceZ,key=lambda pieceZ: pieceZ[1])
    #placePieceIdx=0
    #for i in range(64):
    #    isInt=False
    #    try:
    #       p=pos[-1]
    #       val=int(p)
    #       isInt=True
    #    except ValueError:
    #        #not an int
    #        isInt=False


    piecesPlaced=[] #random integers for each piece



    #for r in rowsP:

    #    print(r)

    pass
    #env.board.API.turn=False
    #env.render()
    #l=list(env.board.API.legal_moves)

    #m=env.get_legal_moves(env.board)
    #a=env.convertActionToMove(4095)
    #print(f"{l[-1]} - {m[-1]} - {env.convertActionToMove(m[-1])}")
    #env.step(m[-1])
    #env.render()
    #print(env.board)
    #print(env.board.API.is_game_over())
    #print(env.board.API.is())


    #print(env.is_terminal())
    #print(env.winner)

    #env.reset()
    #print("--------------------------")
    env.render()
    m = env.legal_moves_list(env.board)
    move = random.choice(m)
    env.step(move)
    print(env.key)

    while not env.done:
        print(f"p{3-env.next_player} moved:{move}")
        env.render()
        print(env.key)
        #print(list(env.board.API.move_stack))
        env = env.clone()
        m = env.legal_moves_list(env.board)
        move=random.choice(m)
        env.step(move)

        #print("MOVES----------")
        #print(env.get_legal_moves(env.board))

        #print(list(env.board.API.legal_moves))
        #print(env.get_result())
        #b = env.board.clone()


    print(env.render())
    print(f"Result:{env.get_result()}")
    print(env.board.planes)

    print(env.legal_moves_list(env.board))
    print(env.get_legal_moves_mask(env.board))

    #c=env.find_correct_moves(own,en)
    #print(f"CorrectMoves:{c}")
    #print(f"bittoarray {bit_to_array(c,env.width)}")
