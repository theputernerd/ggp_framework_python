from player.player_inherit_from import i_Player
import sys
from env.env_inherit_from import Environment

class Human_Player(i_Player):
    def __init__(self,env,playing_as=1):
        super().__init__(env, playing_as, name="Human", shortName=f"H_p{playing_as}")

        pass

    def get_move(self, env:Environment):
        while True:
            try:
                m = env.legal_moves_list()
                env.render()
                movement = input(f"\nEnter your movement\n {m}: ")
                movement = int(movement) #Always an int for now?
                #TODO have to change thi if need x,,y cooards
                if movement in m:
                    return movement
                else:
                    print("That is NOT a valid movement :(.")
            except:
                print("That is NOT a valid movement :(.")

    def new_game(self):
        pass

    def finished_game(self, key,winner):
        pass

from env.connect4_env import Connect4Env
from env.tictactoe import TTT

from lib.helpers import playOneGame

if __name__ == '__main__':

    env=Connect4Env()
    while True:
        p1=Human_Player(env,1)
        p2=Human_Player(env,2)
        winner=playOneGame(env,p1,p2)
        env.render()
        env.reset()