from abc import ABCMeta, abstractmethod
class i_Player(object,metaclass=ABCMeta):
    @abstractmethod
    def __init__(self,env,playing_as,name,shortName=None):
        """
        Initializes the player.
        :param env: a game environment object. i.e. the game it is playing.
        :param playing_as: which player this player is playing as. i.e. 1 or 2
        :param name: The name for this player. This is handy if you are saving files and stuff
        :param shortName: The short name of the player. This is useful for screen output if you want.
        """
        self.env=env #this is the environment. It has predefined methods with the Player class calls.
        self.playing_as=playing_as
        self.name = name
        if shortName==None:
            self.shortName=name[0:5]
        else:
            self.shortName = shortName

    @abstractmethod
    def get_move(self,board):
        description="You need to override the do_move method in player. It needs to return an integer representing the move chosen based on the given board."
        raise Exception('Player method not over-ridden',__file__,description)

    @abstractmethod
    def new_game(self):
        """
        This is called when a new game is started. This allows your agent to prepare for a new game if needed
        :return:
        """
        description="You need to override the new_game method in player. It will be called when a new game is started, and can be used to signal to your player to reset."
        raise Exception('Player method not over-ridden',__file__,description)

    @abstractmethod
    def finished_game(self, key,winner):
        """
        This is called when the game has ended.
        :param winner: The winner 1 or 2 of the game or 0 if not ended or 3 is a draw

        :return:
        """
        description="You need to override the finished_game method in player. It will be called when a game is finished and signal to your player the winner and final board."
        raise Exception('Player method not over-ridden',__file__,description)