from player.player_inherit_from import i_Player
import random
from env.env_inherit_from import Environment
class Random_Player(i_Player):
    def __init__(self,env,playing_as=1):
        shortName = f"Rnd_p{playing_as}"
        name=shortName
        super().__init__(env,playing_as,name,shortName)
        pass

    def get_move(self, env:Environment):
        m = env.legal_moves_list()
        move=random.choice(m)
        return move

    def finished_game(self,key,winner):
        pass

    def new_game(self):
        pass

from env.connect4_env import Connect4Env
from env.tictactoe import TTT

from lib.helpers import playOneGame

if __name__ == '__main__':

    env=Connect4Env()
    while True:
        p1=Random_Player(env,1)
        p2=Random_Player(env,2)
        winner=playOneGame(env,p1,p2)
        env.render()
        env.reset()